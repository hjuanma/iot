from flask import Flask, render_template, request, url_for, redirect, Response, jsonify
from flask_sqlalchemy import SQLAlchemy
from datetime import datetime, time

server = Flask(__name__)

server.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///database/iot.db'

db = SQLAlchemy(server)


class  Sensor(db.Model):
    id = db.Column(db.Integer, primary_key = True)
    active = db.Column(db.Boolean)
    

class Record(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    sensorId = db.Column(db.Integer)
    temperature = db.Column(db.String(200))
    humidity = db.Column(db.Float)
    longitude = db.Column(db.Float)
    latitude = db.Column(db.Float)
    date = db.Column(db.DateTime)


##### RECORDS #####
@server.route('/records')
def records():
    records = Record.query.all()
    return render_template('records.html', records=records)


@server.route('/records/<id>', methods=['GET'])
def getRecord(id):
    record = Record.query.all().filter(Record.id == id)
    response = jsonify({
            'id': str(id),
            'sensor' : record.sensorId,
            'latitud': record.latitude,
            'longitud': record.longitude,
            'temperatura': record.temperature,
            'humedad' : record.humidity,
            'fecha' : record.date
        })
    if len(response) > 0:
        return Response(response, mimetype='application/json')
    else:
        return {'message': 'Error'}

@server.route('/records', methods=['POST'])
def creteRecord():
    # Recivir datos
    sensor = request.json['sensor']
    latitud = request.json['latitud']
    longitud = request.json['longitud']
    temperatura = request.json['temperatura']
    humedad = request.json['humedad']
    fecha = datetime.now()

    if latitud and temperatura and longitud and humedad:
        
        record = Record(sensorId = sensor, latitude = latitud, longitude = longitud, temperature =  temperatura, humidity = humedad, date =  fecha)
        
        db.session.add(record)
        db.session.commit()

        response = jsonify({
            '_id': str(id),
            'sensor' : sensor,
            'latitud': latitud,
            'longitud': longitud,
            'temperatura': temperatura,
            'fecha' : fecha
        })
        response.status_code = 201
        return response
    else:
        return {'message': 'Error'}

##### SENSORES #####
@server.route('/create_sensor', methods=['POST'])
def createSensor():
    sensor = Record(content=request.form['content'], done=False)
    if len(sensor.content) > 0:
        db.session.add(sensor)
        db.session.commit()
    return redirect(url_for('records'))

@server.route('/activate/<int:id>')
def done(id):
    sensor = Sensor.query.filter_by(id=id).first()
    sensor.active = not sensor.active
    db.session.commit()
    return redirect(url_for('records'))


if __name__ == '__main__':
    server.run(debug=True, host='0.0.0.0', port=80)