import dash
import dash_core_components as dcc
import dash_html_components as html
import plotly.graph_objs as go
import pandas as pd
import plotly.express as px

import flaskServer
from flaskServer import server

app = dash.Dash(server=server)

json = pd.read_json('Datos_SIATA_Aire_pm25.json', convert_dates=True)

m = []
for i in range(21):
    m.append(json.datos[i][-1].get('valor'))

fig = go.Figure(go.Densitymapbox(lat=json.latitud.values.tolist(), lon=json.longitud.values.tolist(), z= m, radius = 100))

fig.update_layout(mapbox_style="stamen-terrain", mapbox_center_lon=-75.589900, mapbox_center_lat=6.240737, mapbox_zoom=20)
fig.update_layout(margin={'l': 0, 'r': 0, 'b': 0, 't': 0})

app.layout = html.Div([
        html.H1('Alerta AQI'),
        dcc.Graph(id='map', figure=fig)
    ])


if __name__ == '__main__':
    app.run_server(debug=True)
