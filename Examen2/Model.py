from enum import Enum, auto

class PollutantType(Enum):
    OZONE = auto()
    PM25 = auto()
    PM10 = auto()


def CalculateAQI(pollutant, type):
    calc = Calculator()
    sw = {
        PollutantType.OZONE: calc.Ozone(pollutant),
        PollutantType.PM25: calc.PM25(pollutant),
        PollutantType.PM10: calc.PM10(pollutant),
    }
    # print (sw.get(type, "Invalid Pollutant"))
    return print (sw.get(type, "Invalid Pollutant"))


class Calculator:
    # # Valor minimo del contaminante
    ConcL = 1
    # # Valor maximo del contaminante
    ConcH = 1
    # # Valor minimo del AQI
    AQIL = 1
    # # Valor maximo del AQI
    AQIH = 1

    def Ozone(self, pollutant):
        self.POLLUTANT = pollutant
        if (pollutant < 0.054):
            self.ConcL = 0
            self.ConcH = 0.054
            self.AQIL = 0
            self.AQIH = 50

        elif (pollutant < 0.070):
            self.ConcL = 0.054
            self.ConcH = 0.070
            self.AQIL = 51
            self.AQIH = 100

        elif (pollutant < 0.085):
            self.ConcL = 0.071
            self.ConcH = 0.085
            self.AQIL = 101
            self.AQIH = 150

        elif (pollutant < 0.105):
            self.ConcL = 0.085
            self.ConcH = 0.105
            self.AQIL = 151
            self.AQIH = 200

        else:
            self.ConcL = 0.105
            self.ConcH = 0.2
            self.AQIL = 201
            self.AQIH = 300
        
        return self.AQI()

    def PM25(self, pollutant):
        self.POLLUTANT = pollutant
        if (pollutant < 12.0):
            self.ConcL = 0
            self.ConcH = 12.0
            self.AQIL = 0
            self.AQIH = 50

        elif (pollutant < 35.4):
            self.ConcL = 12.1
            self.ConcH = 35.4
            self.AQIL = 51
            self.AQIH = 100

        elif (pollutant < 55.4):
            self.ConcL = 35.5
            self.ConcH = 55.4
            self.AQIL = 101
            self.AQIH = 150

        elif (pollutant < 150.4):
            self.ConcL = 55.5
            self.ConcH = 150.4
            self.AQIL = 151
            self.AQIH = 200

        elif (pollutant < 250.4):
            self.ConcL =  150.5
            self.ConcH = 250.4
            self.AQIL = 201
            self.AQIH = 300

        else:
            self.ConcL = 250.5
            self.ConcH = 500.4
            self.AQIL = 301
            self.AQIH = 500
        
        return self.AQI()

    def PM10(self, pollutant):
        self.POLLUTANT = pollutant
        if (pollutant < 54):
            self.ConcL = 0
            self.ConcH = 54
            self.AQIL = 0
            self.AQIH = 50

        elif (pollutant < 154):
            self.ConcL = 55
            self.ConcH = 154
            self.AQIL = 51
            self.AQIH = 100

        elif (pollutant < 254):
            self.ConcL = 155
            self.ConcH = 254
            self.AQIL = 101
            self.AQIH = 150

        elif (pollutant < 354):
            self.ConcL = 255
            self.ConcH = 354
            self.AQIL = 151
            self.AQIH = 200

        elif (pollutant < 424):
            self.ConcL =  354
            self.ConcH = 424
            self.AQIL = 201
            self.AQIH = 300

        else:
            self.ConcL = 425
            self.ConcH = 604
            self.AQIL = 301
            self.AQIH = 500
        
        return self.AQI()

    def AQI(self):
        result = ((self.AQIH - self.AQIL)/(self.ConcH - self.ConcL))*(self.POLLUTANT - self.ConcL) + self.AQIL
        return result


def ValidateData(data):
    if(data > 0 and data < 15000):
        return True

    return False

    
# CalculateAQI(45, PollutantType.PM25)