import pandas as pd
json = pd.read_json('Datos_SIATA_Aire_pm25.json', convert_dates=True)

latitudes = json.latitud.values.tolist()
longitudes = json.longitud.values.tolist()
fechas = json.datos[1][-1].get('fecha')

m = []

for i in range(21):
    m.append(json.datos[i][-1].get('valor'))

