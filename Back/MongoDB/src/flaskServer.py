from flask import Flask, request, jsonify, Response
from flask_pymongo import PyMongo
from werkzeug.security import generate_password_hash, check_password_hash
from bson import json_util
from bson.objectid import ObjectId

server = Flask(__name__)
server.config['MONGO_URI'] = 'mongodb://sa:a123456@ds241268.mlab.com:41268/testflask?retryWrites=false'

mongo = PyMongo(server)

##### USUARIOS #####
@server.route('/users', methods=['GET'])
def getUsers():
    users = mongo.db.users.find()
    response = json_util.dumps(users)
    if len(response) > 0:
        return Response(response, mimetype='application/json')
    else:
        return {'message': 'Error'}

@server.route('/users/<id>', methods=['GET'])
def getUser(id):
    user = mongo.db.users.find_one({'_id': ObjectId(id)})
    response = json_util.dumps(user)
    if len(response) > 0:
        return Response(response, mimetype='application/json')
    else:
        return {'message': 'Error'}

@server.route('/users/<id>', methods=['DELETE'])
def deletUser(id):
    mongo.db.users.delete_one({'_id': ObjectId(id)})
    return {'message': 'Ususario eliminado'}

@server.route('/users', methods=['POST'])
def create_user():
    # Receiving Data
    username = request.json['username']
    email = request.json['email']
    password = request.json['password']

    if username and email and password:
        hashed_password = generate_password_hash(password)
        id = mongo.db.users.insert(
            {'username': username, 'email': email, 'password': hashed_password})
        response = jsonify({
            '_id': str(id),
            'username': username,
            'password': hashed_password,
            'email': email
        })
        response.status_code = 201
        return response
    else:
        return {'message': 'Error'}

@server.route('/users/<id>', methods=['PUT'])
def updateUser(id):
    username = request.json['username']
    email = request.json['email']
    password = request.json['password']
    if username and email and password:
        hashed_password = generate_password_hash(password)
        mongo.db.users.update_one(
            {'_id' : ObjectId(id)},
            {'$set': {
                'username': username, 
                'email': email, 
                'password': hashed_password
            }})
        response = jsonify({
            '_id': str(id),
            'username': username,
            'password': hashed_password,
            'email': email
        })
        response.status_code = 201
        return response
    else:
        return {'message': 'Error'}



##### RECORDS #####
@server.route('/records', methods=['GET'])
def getRecords():
    records = mongo.db.records.find()
    response = json_util.dumps(records)
    if len(response)> 0:
        return Response(response, mimetype='application/json')
    else:
        return {'message': 'Error'}

@server.route('/records/<id>', methods=['GET'])
def getRecord(id):
    records = mongo.db.records.find_one({'_id': ObjectId(id)})
    response = json_util.dumps(records)
    if len(response) > 0:
        return Response(response, mimetype='application/json')
    else:
        return {'message': 'Error'}

@server.route('/records', methods=['POST'])
def creteUser():
    # Recivir datos
    sensor = request.json['sensor']
    latitud = request.json['latitud']
    longitud = request.json['longitud']
    temperatura = request.json['temperatura']

    if latitud and temperatura and longitud:

        record = {'sensor': sensor, 'latitud': latitud,
                  'longitud': longitud, 'temperatura': temperatura}
        mongo.db.records.insert(record)
        response = jsonify({
            '_id': str(id),
            'sensor' : sensor,
            'latitud': latitud,
            'longitud': longitud,
            'temperatura': temperatura
        })
        response.status_code = 201
        return response
    else:
        return {'message': 'Error'}

@server.errorhandler(404)
def not_found(error = None):
    response = jsonify({
        'message':'Respuesta no encontrada' + request.url,
        'status' : 404
    })

