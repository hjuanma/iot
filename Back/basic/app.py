from flask import Flask, jsonify, request
from sensor import sensors

app = Flask(__name__)

@app.route('/ping')
def ping():
    return jsonify({"message":"pong"})

@app.route('/sensor')
def getSensors():
    return jsonify(sensors)


@app.route('/sensor/<int:id>')
def getSensor(id):
    sensorResponse = [sensor for sensor in sensors if sensor['id'] == id]

    if(len(sensorResponse)>0):
        return jsonify(sensorResponse)
    else:
        return jsonify({'meassge': 'sensor no encontrado'})

@app.route('/sensor', methods=['POST'])
def addRecord():
    newRecord = {
        "id" : request.json['id'],
        "latitud" : request.json['latitud'],
        "longitud" : request.json['longitud'],
        "temperatura" : request.json['temperatura']
    }

    sensors.append(newRecord)
    return jsonify({'message':'nuevo registro agregado', 'registros': sensors})

@app.route('/sensor/<int:id>', methods = ['PUT'])
def editSensor(id):
    sensorResponse = [sensor for sensor in sensors if sensor['id'] == id]
    if (len(sensorResponse) > 0):
        sensorResponse[0]['id'] = id
        sensorResponse[0]['latitud'] = request.json['latitud']
        sensorResponse[0]['longitud'] = request.json['longitud']
        sensorResponse[0]['temperatura'] = request.json['temperatura']
        return jsonify({
            'message' : 'sensor actualizado',
            'registros': sensors 
        })

    else:
        return jsonify({"message" : "no se encontro el sensor"})

@app.route('/sensor/<int:id>', methods = ['DELETE'])
def deleteSensor(id):
    sensorResponse = [sensor for sensor in sensors if sensor['id'] == id]

    if len(sensorResponse)> 0:
        sensors.remove(sensorResponse[0])
        return jsonify({'message':'sensor eliminado',
        'sensores':sensors})
    else:
        return jsonify({'messge' : 'el sensor no se pudo encontrar'})

if __name__ == 'main':
    app.run(debug = True, port=4000)